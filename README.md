# de-identification-rules

Utilize this repository for storing the de-identification rules template. The template must be filled by the project and approved as indicated in the DTR process.

Templates:
- De-Identification-rules-V2_20230926: Extracted from the archived Template Use case evaluation and risk assessment v1.0
- De-Identification-rules-V3_20250129: Extracted from the current Template Use case evaluation and risk assessment v2.0
    - The updated template is currently being modified.
